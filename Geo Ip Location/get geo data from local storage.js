var geoDataPushed = localStorage.getItem("geoDataPushed");

if(geoDataPushed == true){
    var getGeoData = localStorage.getItem("geoData");
    var cleaData = JSON.parse(getGeoData);
    
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        event: "geoDataAvailable",
        geoData: cleaData,
    })
}else{
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
        event: "geoDataNotAvailable",
    })
}
