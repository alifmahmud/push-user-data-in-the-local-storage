
var geoData = {
    ip: "103.143.127.14",
    continent_code: "AS",
    continent_name: "Asia",
    country_code2: "BD",
    country_code3: "BGD",
    country_name: "Bangladesh",
    country_name_official: "People’s Republic of Bangladesh",
    country_capital: "Dhaka",
    state_prov: "Dhaka Division",
    state_code: "BD-C",
    district: "",
    city: "Bhulta",
    zipcode: "1462",
    latitude: "23.78580",
    longitude: "90.56500",
    is_eu: false,
    calling_code: "+880",
    country_tld: ".bd",
    languages: "bn-BD,en",
    country_flag: "https://ipgeolocation.io/static/flags/bd_64.png",
    geoname_id: "11285059",
    isp: "Freedom Online",
    connection_type: "",
    organization: "Freedom Online",
    currency: {code: "BDT", name: "Bangladeshi Taka", symbol: "৳"},
    time_zone: {
      name: "Asia/Dhaka",
      offset: 6,
      offset_with_dst: 6,
      current_time: "2023-11-22 06:09:03.692+0600",
      current_time_unix: 1700611743.692,
      is_dst: false,
      dst_savings: 0
    }
  };

var geoDataPushed = true;

localStorage.setItem("geoData", JSON.stringify(geoData));
localStorage.setItem("geoDataPushed", geoDataPushed);

window.dataLayer = window.dataLayer || [];
window.dataLayer.push({
     event: "GeoDatPushed",
})
